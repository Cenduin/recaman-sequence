% Example code that prompts the user for options to generate and display
% Recam�n's sequence

iterations = input('How many iterations to generate? ');
sequence = generateRecaman(iterations);

displaySeq = input('Display sequence? Y/N [Y]: ', 's');
if isempty(displaySeq)
    displaySeq = 'Y';
end
if(displaySeq=='Y')
   disp(sequence) 
end
plotSeq = input('Plot sequence? Y/N [Y]: ', 's');
if isempty(plotSeq)
    plotSeq = 'Y';
end
if (plotSeq=='Y')
   plotRecaman(sequence); 
end