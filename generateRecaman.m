function sequence = generateRecaman(iterations)
% generateRecaman   Generates Recam�n's sequence
%   Usage: generateRecaman(iterations)
%   See https://oeis.org/A005132 for information on the sequence
    
    % Allocate array to hold sequence
    sequence = zeros(1, iterations); 
    % Loop through all iterations starting at the second to avoid underflow
    for n = 2:iterations 
        % Check if subtraction from current value results in a negative or
        % previously used value. If either of those are true, then add
        % iteration counter to current value, otherwise subtract it to get
        % the next value in the sequence
        if (sequence(n-1) - (n-1) <= 0) || ismember(sequence(n-1) - (n - 1), sequence)
            sequence(n) = sequence(n-1) + (n-1);
        else
            sequence(n) = sequence(n-1) - (n-1);
        end
    end
end
