function plotRecaman(sequence)
% plotRecaman   plots a row vector
%   Usage: plotRecaman(sequence)
%   Imitates plotting of Recam�n's sequence as seen in this video: 
%   https://www.youtube.com/watch?v=FGC5TdIiT9U
%   Can actually be used for any arbitrary row vector of real numbers

    % Get length of vector
    length = size(sequence, 2);
    % Check we have a valid input
    if ~ isrow(sequence)
        error("Input must be a row vector")
    elseif length == 1
        error("Sequence must have more than one member to be plotted")
    else
        % Set up plotting environment
        figure;
        hold on;
        % Hide axes
        axis off;
        % Make sure the figure doesn't get squashed
        axis equal;
        % Maximise display
        set(gcf, 'Position', get(0, 'Screensize'));
        % Set background to white
        set(gcf, 'color', 'w');
        % Generate vector of angles to plot the semicircles later
        theta = linspace(pi / 2, - pi / 2, 40);
        % Initialise the direction variable for flipping the semicircles
        direction = 1;
        % Loop through input
        for i = 1 : (length - 1)
            % Generate x and y vectors to draw a semicircle between ith and
            % (i+1)th values in the sequence
            x = abs(sequence(i) - sequence(i + 1)) / 2 * sin(theta) + (sequence(i) + sequence(i + 1)) / 2;
            y = direction * abs(sequence(i) - sequence(i + 1)) / 2 * cos(theta);
            % Plot semicircle
            plot(x, y, 'k');
            % Change direction for next semicircle
            direction = - direction;
        end
    end
end
